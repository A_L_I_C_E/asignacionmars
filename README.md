> Fabricio Parra 30.837.006 \
> Elio Guarate 28.182.524

Este repositorio contiene un informe en PDF que explica lo siguiente:
* Un tutorial de Mips32 Mars

* La diferencia entre los ejercicios del libro de Patterson y los códigos de la página de Mars32 Mips

* Cómo hacer funcionar un ejemplo del libro

También hay 3 archivos asm de los códigos de ejemplo de la página de Mips Mars, otro archivo asm de la suma, y el archivo asm del ordenamiento.
  
**Información del informe:**

El informe se divide en tres secciones principales:

* **Sección 1:** Tutorial de Mips32 Mars

Esta sección proporciona una introducción a Mips32 Mars, un entorno de desarrollo integrado (IDE) para la arquitectura de microprocesadores Mips. El informe cubre los siguientes temas:

* Cómo instalar Mips32 Mars

* Cómo crear un nuevo proyecto

* Cómo escribir código Mips

* Cómo compilar y ejecutar código Mips

* **Sección 2:** Diferencias entre los ejercicios del libro de Patterson y los códigos de la página de Mars32 Mips

Esta sección explica las diferencias entre los ejercicios del libro de Patterson y los códigos de ejemplo de la página de Mars32 Mips. El informe cubre los siguientes temas:

* Diferencias en la sintaxis del lenguaje Mips

* Diferencias en la organización de los archivos

* Diferencias en las entradas y salidas

* **Sección 3:** Cómo hacer funcionar un ejemplo del libro

Esta sección proporciona instrucciones paso a paso sobre cómo hacer funcionar un ejemplo del libro de Patterson. El informe cubre los siguientes temas:

* Cómo descargar el código del libro

* Cómo modificar el código para que funcione con Mips32 Mars

* Cómo compilar y ejecutar el código modificado

**Información de los archivos asm:**

Los siguientes archivos asm se proporcionan en este repositorio:

* **Ejercicio122.asm:** Código asm para una suma recursiva de si misma

* **bubble_sort.asm:** Código asm para el ordenamiento de una lista de números

Estos archivos se pueden utilizar para aprender a escribir código Mips o para depurar los ejemplos del libro.

Este repositorio proporciona una serie de recursos para aprender a programar en Mips32 Mars. El informe, los archivos asm y los ejemplos del libro pueden ser utilizados por estudiantes, investigadores y desarrolladores de software.
