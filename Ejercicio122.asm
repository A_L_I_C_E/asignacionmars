###################################################################################3
#
#
#	Algoritmo de la pagina 122 del Libro del Patterson
#	Llamada de cola (Tail Call)
#

	
	.data
mensaje: .asciiz "Ingrese el numero: "
mensaje2: .asciiz "El numero resultado es: "
	.text
	la $a0, mensaje  			#Indicar la direccion de memoria del mensaje1
	li $v0, 4				#Indicar que se va a imprimir una cadena
	syscall
	#Se imprime el mensaje1
	
	li $v0, 5				#Indicar que se va a leer un entero
	syscall
	#Se lee el numero (n)
	
	move $s0, $v0 				#En s0 está el numero 1 (n)
	li $t0, 0				#Inicializando el Acumulador en t0
	move $s1, $t0 				#En s1 está inicializado el acumulador (acc)
	jal sum 				#Brincar a "sum" y guardar posicion en $ra
	
	la $a0, mensaje2			#Al regresar, guardar la Dirección de Memoria de mensaje2
	li $v0, 4				#Indicar que se va a imprimir una cadena
	syscall
	#Imprimir mensaje3
	
	move $a0, $s1 				#Almacenar el resultado de toda la suma en $a0 para imprimirlo
	li $v0, 1				#Indicar que se va a imprimir un numero entero
	syscall
	#Imprimir el numero
	
	li $v0, 10				#Indicar la salida del programa
	syscall
	#Salir del sistema
	
	
sum:	slti $a0, $s0, 1			#Comprueba si n <= 0 // $a0 = n <= 0 ? 1 : 0
	bne $a0, $zero, sum_exit 		#Salto a sum_exit si n<=0
	add $s1, $s1, $s0 			#Suma "n" a "acc"
	addi $s0, $s0, -1			#Decrementa "n"
	j sum		   			#Salto a "sum"
	
sum_exit:	
		jr $ra		#retorno de rutina

	